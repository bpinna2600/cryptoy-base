import hashlib
import os
from random import (
    Random,
)

import names


def hash_password(password: str) -> str:
    return hashlib.sha3_256(password.encode()).hexdigest()


def random_salt() -> str:
    return bytes.hex(os.urandom(32))


def generate_users_and_password_hashes(
    passwords: list[str], count: int = 32
) -> dict[str, str]:
    rng = Random()  # noqa: S311

    users_and_password_hashes = {
        names.get_full_name(): hash_password(rng.choice(passwords))
        for _i in range(count)
    }
    return users_and_password_hashes


def attack(passwords: list[str], passwords_database: dict[str, str]) -> dict[str, str]:
    users_and_passwords = {}

    hashed_passwords = {password: hash_password(password) for password in passwords}

    # Boucle sur le json
    for user, password_hash in passwords_database.items():
        print(user)
        # On teste les mot de bases de la base de données contre la liste de mot de passe hachée
        for password, hashed_password in hashed_passwords.items():
            if password_hash == hashed_password:
                # Ajout de l'utilisateur et du mot-de-passe dans le dictionnaire de résultat
                print(password_hash)
                users_and_passwords[user] = password
                break

    return users_and_passwords



def fix(
    # A implémenter
    # Doit calculer une nouvelle base de donnée ou chaque élement est un dictionnaire de la forme:
    # {
    #     "password_hash": H,
    #     "password_salt": S,
    # }
    # tel que H = hash_password(S + password)

    passwords: list[str], passwords_database: dict[str, str]
) -> dict[str, dict[str, str]]:
    users_and_passwords = attack(passwords, passwords_database)

    users_and_salt = {}
    new_database = {}

    for user, password in users_and_passwords.items():
        salt = random_salt()
        salted_password_hash = hash_password(salt + password)
        new_database[user] = {"password_hash": salted_password_hash, "password_salt": salt}

    

    return new_database


def authenticate(username: str, password: str, database: dict[str, dict[str, str]]) -> bool:
    # Vérifie si l'utilisateur existe dans la base de données
    if username not in database:
        return False

    # Récupère le sel et le hachage du mot de passe pour cet utilisateur
    user_data = database[username]
    salt = user_data["password_salt"]
    stored_password_hash = user_data["password_hash"]

    # Hache le mot de passe fourni avec le sel
    provided_password_hash = hash_password(salt + password)

    # Vérifie si le hachage du mot de passe fourni correspond au hachage du mot de passe stocké
    return provided_password_hash == stored_password_hash

